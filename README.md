# Test Uhmane - Fábio Souza
### [fsouza.me@gmail.com](fsouza.me@gmail.com)

## Instalation


### DB table for the tests
```sh
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;
```

### Insert two contacts for tests
```sh
INSERT INTO `contact` (`id`, `first_name`, `last_name`, `email`, `phone`) VALUES
(1, 'Test', 'One', 'test@gmail.com', '(11) 99999-9999'),
(2, 'Test', 'Two', 'test2@gmail.com', '(11) 98888-8888');
```

## Steps

### 1. Clone the project
```sh
git clone git@bitbucket.org:fbsouzas/uhmane-test.git
```

### 2. Go to the project directory
```sh
cd /path/to/project/
```

### 3. Copy the parameters file and configure
```sh
cp parameters.php.dist parameters.php
```

### 4. Install the dependencies
```sh
composer install
```
(If not has **composer** visit [getcomposer.org](http://getcomposer.org) for more details)

### 5. Started php server
```sh
php -S localhost:8000
```

## Install HTTPie for tests
### For more details, visit [https://httpie.org/](https://httpie.org/)

## Examples for test API
### GET (all contacts)
```sh
http GET http://localhost:8000/api/contacts/
```

### GET (contact by id)
```sh
http GET http://localhost:8000/api/contacts/{id}
```

### POST
```sh
http POST http://localhost:8000/api/contacts/ first_name='First Name' last_name='Last Name' email='email@email.com' phone='\(11\) 99999-9999'
```

### PUT
```sh
http PUT http://localhost:8000/api/contacts/{id} first_name='First Name' last_name='Last Name' email='email@email.com' phone='\(11\) 99999-9999'
```

### DELETE
```sh
http DELETE http://localhost:8000/api/contacts/{id}
```
