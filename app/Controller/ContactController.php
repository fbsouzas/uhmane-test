<?php

namespace App\Controller;

use \App\Model\Contact;

class ContactController
{
    /**
     * Return all contacts
     *
     * @return array (with contact) | array (with error)
     */
    public function index()
    {
        try {
            $contacts = Contact::findAll();
        } catch (\Exception $e) {
            return array(
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            );
        }

        return array('contacts' => $contacts);
    }

    /**
     * Return contact by id
     *
     * @param integer $id (contact id)
     * @return array (with contact) | array (with error)
     */
    public function show($id)
    {
        try {
            $contact = Contact::findBy($id);
        } catch (\Exception $e) {
            return array(
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            );
        }

        return array('contact' => $contact);
    }

    /**
     * Create an new contact
     *
     * @param array $paramaters  (with data of form)
     * @return array
     */
    public function create(Array $parameters)
    {
        try {
            $contact = Contact::save($parameters);
        } catch (\Exception $e) {
            return array(
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            );
        }

        return array('contact' => $contact);
    }

    /**
     * Updates an existing contact
     *
     * @param array $paramaters  (with data of form)
     * @param interger $id (contact id)
     * @return array
     */
    public function update(Array $parameters, $id)
    {
        try {
            $contact = Contact::update($parameters, $id);
        } catch (\Exception $e) {
            return array(
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            );
        }

        return array('contact' => $contact);
    }

    /**
     * Delete an existing contact
     *
     * @param interger $id (contact id)
     * @return array
     */
    public function delete($id)
    {
        try {
            $contact = Contact::delete($id);
        } catch (\Exception $e) {
            return array(
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            );
        }

        return array('contact' => $contact);
    }
}
