<?php

namespace App\Model;

use App\ConnectionDatabase;

class Contact
{
    /**
     * Query in the database for all contacts
     *
     * @return array (with all contacts objects)
     */
    public static function findAll()
    {
        $conn = new ConnectionDatabase();
        $sql = 'SELECT * FROM contact ORDER BY first_name ASC';
        $stmt = $conn->prepare($sql);

        if (! $stmt->execute()) {
            throw new \Exception("Error Processing Request.", 500);
        }

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Query in the database contact by id
     *
     * @return array (with the contact object)
     */
    public static function findBy($id)
    {
        $conn = new ConnectionDatabase();
        $sql = 'SELECT * FROM contact WHERE id = :id ORDER BY first_name ASC';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);

        if (! $stmt->execute()) {
            throw new \Exception("Error Processing Request.", 500);
        }

        if (! $stmt->rowCount()) {
            throw new \Exception("Contact Not Found.", 404);
        }

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Save in the database an new contact
     *
     * @param array $paramaters (with all data submit of form)
     * @return array (with http status and message)
     */
    public static function save(Array $parameters)
    {
        $conn = new ConnectionDatabase();

        $sql =
            'INSERT INTO
                contact(first_name, last_name, email, phone)
            VALUES
                (:first_name, :last_name, :email, :phone)'
        ;

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':first_name', $parameters['first_name']);
        $stmt->bindParam(':last_name', $parameters['last_name']);
        $stmt->bindParam(':email', $parameters['email']);
        $stmt->bindParam(':phone', $parameters['phone']);

        if (! self::isNull($parameters)) {
            throw new \Exception("Bad Request - Some Required Parameter Was Not Sent Or Is Invalid.", 400);
        }

        if (! $stmt->execute()) {
            throw new \Exception("Error Processing Request.", 500);
        }

        return array(
            'code' => 201,
            'message' => 'Created object.',
        );
    }

    /**
     * Updates in the database an existing contact
     *
     * @param array $paramaters (with all data submit of form)
     * @return array (with http status and message)
     */
    public static function update(Array $parameters, $id)
    {
        $conn = new ConnectionDatabase();

        $sql =
            'UPDATE
                contact
            SET
                first_name = :first_name, last_name = :last_name, email = :email, phone = :phone
            WHERE
                id = :id'
        ;

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':first_name', $parameters['first_name']);
        $stmt->bindParam(':last_name', $parameters['last_name']);
        $stmt->bindParam(':email', $parameters['email']);
        $stmt->bindParam(':phone', $parameters['phone']);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);

        if (! self::isNull($parameters)) {
            throw new \Exception("Bad Request - Some Required Parameter Was Not Sent Or Is Invalid.", 400);
        }

        if (! $stmt->execute()) {
            throw new \Exception("Error Processing Request.", 500);
        }

        return array(
            'code' => 200,
            'message' => 'Updated object.',
        );
    }

    /**
     * Delete in the database an existing contact by id
     *
     * @param integer $id (contact id)
     * @return array (with http status and message)
     */
    public static function delete($id)
    {
        $conn = new ConnectionDatabase();
        $sql = 'DELETE FROM contact WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);

        if (! $stmt->execute()) {
            throw new \Exception("Error Processing Request.", 500);
        }

        if (! $stmt->rowCount()) {
            throw new \Exception("Contact Not Found.", 404);
        }

        return array(
            'code' => 200,
            'message' => 'Deleted object.',
        );
    }

    /**
     * Validates if fields are null
     *
     * @param array $parameters (with all data submit of form)
     * @return boolean
     */
    private static function isNull(Array $parameters)
    {
        return $parameters['first_name'] && $parameters['last_name'] && $parameters['email'] && $parameters['phone']
            ? true
            : false;
    }
}
