<?php

require 'vendor/autoload.php';
require 'parameters.php';

$app = new \Slim\App([
		'settings' => [ 'displayErrorDetails' => true ]
	]);

$container = $app->getContainer();
$contactController = new \App\Controller\ContactController();
$container['view'] = function ($container) {
    return new \Slim\Views\PhpRenderer('app/Views/');
};

$app->group('/api/contacts',function() use ($app, $contactController) {
    $app->get('/', function($request, $response, $args) use ($app, $contactController) {
        $contacts = $contactController->index();

        if (isset($contacts['code'])) {
            $response = $response->withStatus($contacts['code']);
        }

        return $this->view->render($response, 'template.php', [
            'data' => $contacts,
        ]);
    });

    $app->get('/{id}', function($request, $response, $args) use ($app, $contactController) {
        $contact = $contactController->show($args['id']);

        if (isset($contact['code'])) {
            $response = $response->withStatus($contact['code']);
        }

        return $this->view->render($response, 'template.php', [
            'data' => $contact,
        ]);
    });

    $app->post('/', function($request, $response, $args) use ($app, $contactController) {
        $contact = $contactController->create($request->getParsedBody());
        $response = $response->withStatus(201);

        if (isset($contact['code'])) {
            $response = $response->withStatus($contact['code']);
        }

        return $this->view->render($response, 'template.php', [
            'data' => $contact,
        ]);
    });

    $app->put('/{id}', function($request, $response, $args) use ($app, $contactController) {
        $contact = $contactController->update($request->getParsedBody(), $args['id']);

        if (isset($contact['code'])) {
            $response = $response->withStatus($contact['code']);
        }

        return $this->view->render($response, 'template.php', [
            'data' => $contact,
        ]);
    });

    $app->delete('/{id}', function($request, $response, $args) use ($app, $contactController) {
        $contact = $contactController->delete($args['id']);

        if (isset($contact['code'])) {
            $response = $response->withStatus($contact['code']);
        }

        return $this->view->render($response, 'template.php', [
            'data' => $contact,
        ]);
    });
});

$app->run();
